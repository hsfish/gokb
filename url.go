package gokb

import (
	"fmt"
	"net"
	nurl "net/url"
	"regexp"
	"sort"
	"strings"
)

// ParseURL no longer needs to be used by clients of this library since supplying a URL as a
// connection string to sql.Open() is now supported:
//
//	sql.Open("kingbase", "kingbase://bob:secret@1.2.3.4:54321/mydb?sslmode=verify-full")
//
// It remains exported here for backwards-compatibility.
//
// ParseURL converts a url to a connection string for driver.Open.
// Example:
//
//	"kingbase://bob:secret@1.2.3.4:54321/mydb?sslmode=verify-full"
//
// converts to:
//
//	"user=bob password=secret host=1.2.3.4 port=54321 dbname=mydb sslmode=verify-full"
//
// A minimal example:
//
//	"kingbase://"
//
// This will be blank, causing driver.Open to use all of the defaults
func ParseURL(url string) (string, error) {

	// 用户名、密码转义
	if strings.Count(url, "@") > 1 {
		matches := regexp.MustCompile(`kingbase://(.+):(.+)@(.*)`).FindStringSubmatch(url)
		if len(matches) == 4 {
			url = fmt.Sprintf("kingbase://%s:%s@%s", nurl.QueryEscape(matches[1]), nurl.QueryEscape(matches[2]), matches[3])
		}
	}

	u, err := nurl.Parse(url)
	if err != nil {
		return "", err
	}

	if u.Scheme != "kingbase" {
		return "", fmt.Errorf("invalid connection protocol: %s", u.Scheme)
	}

	var kvs []string
	escaper := strings.NewReplacer(` `, `\ `, `'`, `\'`, `\`, `\\`)
	accrue := func(k, v string) {
		if v != "" {
			kvs = append(kvs, k+"="+escaper.Replace(v))
		}
	}

	if u.User != nil {
		v := u.User.Username()
		accrue("user", v)

		v, _ = u.User.Password()
		accrue("password", v)
	}

	if host, port, err := net.SplitHostPort(u.Host); err != nil {
		accrue("host", u.Host)
	} else {
		accrue("host", host)
		accrue("port", port)
	}

	if u.Path != "" {
		accrue("dbname", u.Path[1:])
	}

	q := u.Query()
	for k := range q {
		accrue(k, q.Get(k))
	}

	sort.Strings(kvs) // Makes testing easier (not a performance concern)
	return strings.Join(kvs, " "), nil
}
