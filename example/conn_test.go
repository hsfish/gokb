package example

import (
	"database/sql"
	_ "gitea.com/hsfish/gokb"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestConn(t *testing.T) {
	connStr := "kingbase://ops:Gs@db?>>@172.16.20.105:54321/ops?sslmode=disable"
	db, err := sql.Open("kingbase", connStr)
	if !assert.NoError(t, err) {
		return
	}

	rows, err := db.Query(`SELECT VERSION()`)
	if assert.NoError(t, err) {
		t.Log(rows.Columns())
		for rows.Next() {
			a := new(interface{})
			assert.NoError(t, rows.Scan(&a))
			t.Log(*a)
		}
	}

}
